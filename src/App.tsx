// eslint-disable-next-line no-use-before-define
import React from 'react'

import AppRoutes from './Router/AppRoutes'
import AuthCheck from './Components/AuthCheck'
declare global {
  // eslint-disable-next-line no-unused-vars
  interface Window { evaReplace: any; }
}

function App () {
  return (
    <AuthCheck>
      <AppRoutes />
    </AuthCheck>
  )
}

export default App
