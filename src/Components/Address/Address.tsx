// eslint-disable-next-line no-use-before-define
import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { setUserAddress } from '../../Store/actions/main-actions'

function Address () {
  const [address, setAddress] = useState<string>('')
  const { isLoading } = useSelector((state:any) => state.main.apiState)

  const dispatch = useDispatch()

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const value = e.target.value.trim()
    if (!value) {
      return
    }

    setAddress(value)
  }

  const addAddress = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    dispatch(setUserAddress(address))
  }

  return (
    <div className="sign">
      <div className="container">
          <div className="item">
            <form onSubmit={addAddress}>
              <h4 className="mb-4">ثبت آدرس</h4>
              <div className="form-group">
                      <input
                      name="address"
                      type="text"
                      className="form-control"
                      placeholder="شهر خیابان کوچه"
                      onChange={handleChange}
                      required
                      autoFocus
                      />
                      <button className="btn prepend">
                      <i data-eva="person" />
                      </button>
                  </div>
                  <button type="submit" className="btn primary" disabled={isLoading}>
                      {isLoading && 'درحال جستجو...'}
                      {!isLoading && 'ثبت آدرس'}
                  </button>
              </form>
          </div>
      </div>
    </div>
  )
}

export default Address
