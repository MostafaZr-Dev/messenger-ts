// eslint-disable-next-line no-use-before-define
import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'

import { authUser } from '../../Store/actions/main-actions'
import { selectAuthFlag } from '../../Store/selectors/main-selectors'
import Loader from '../Partials/Loader'

function AuthCheck ({ children }: React.PropsWithChildren<{}>) {
  const isAuth = useSelector(selectAuthFlag)

  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(authUser())
  }, [])

  return (
        <>
            {isAuth && <Loader />}
            {!isAuth && <> {children} </>}
        </>
  )
}

export default AuthCheck
