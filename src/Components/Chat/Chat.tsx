// eslint-disable-next-line no-use-before-define
import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import ActiveChat from './Components/ActiveChat'
import { useChatState } from '../Messenger/state/chat-provider'
import { ChatContext } from '../Messenger/state/chat-context'
import { useConversationContext } from '../Messenger/state/conversation-provider'
import { ConversationContext } from '../Messenger/state/conversation-context'
import { selectCurrentUser } from '../../Store/selectors/main-selectors'
import { finishChat, saveMessage, sendMessage } from '../../Store/actions/chat-actions'

function Chat () {
  const { eventManager, socketService } = useChatState() as ChatContext
  const { sender, receiver } = useConversationContext() as ConversationContext

  const me = useSelector(selectCurrentUser)
  const dispatch = useDispatch()

  useEffect(() => {
    window.evaReplace()
    console.log({ sender, receiver })
    if (sender) {
      sender.on('data', (data) => {
        dispatch(sendMessage(data))
      })
    }
    if (receiver) {
      receiver.on('data', (data) => {
        dispatch(sendMessage(data))
      })
    }

    socketService.$on('finishChat', (data: {chatID: string}) => {
      dispatch(finishChat(data.chatID))
    })
  }, [])

  useEffect(() => {
    eventManager.on('message', (data) => {
      console.log(sender)
      if (sender) {
        console.log('data:', data)
        sender.send({
          ...data,
          sender: me.hash,
          createdAt: new Date()
        })
      }

      if (receiver) {
        receiver.send({
          ...data,
          sender: me.hash,
          createdAt: new Date()
        })
      }
      console.log('data:', data)
      dispatch(saveMessage({
        ...data,
        sender: me.hash,
        createdAt: new Date()
      }))
    })

    eventManager.on('finishChat', (data: {chatID:string, to: string}) => {
      dispatch(finishChat(data.chatID))
      socketService.$emit('finishChat', data)
    })
  }, [])

  return (
        <div className='chat'>
            <div className="tab-content">
                <ActiveChat />
            </div>
        </div>
  )
}

export default Chat
