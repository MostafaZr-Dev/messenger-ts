// eslint-disable-next-line no-use-before-define
import React, { useState, useEffect } from 'react'

import Content from '../Content'
import Footer from '../Footer'
import Header from '../Header'
import { useChatState } from '../../../Messenger/state/chat-provider'
import { ChatContext } from '../../../Messenger/state/chat-context'
import { MediaConnection } from 'src/Contracts/Peer'
import MediaService from '../../../../Services/MediaService'
import VideoCall from '../VideoCall'

const mediaService = new MediaService()

function ActiveChat () {
  const [mediaStream, setMediaStream] = useState<MediaStream | null>(null)

  const { eventManager, peerService } = useChatState() as ChatContext

  const startVideoCallHandler = (data: {to: string}) => {
    mediaService.requestMediaPermission({
      video: true,
      audio: true
    }).then(stream => {
      const mediaConnection = peerService.call(data.to, stream)

      mediaConnection.on('stream', remoteStream => {
        setMediaStream(remoteStream)
      })
    }).catch(error => {
      console.log('mediaService Call Error:', error)
    })
  }

  const callReceiveHandler = (call:MediaConnection) => {
    mediaService.requestMediaPermission({
      video: true,
      audio: true
    }).then(stream => {
      call.answer(stream)
      call.on('stream', (remoteStream: MediaStream) => {
        setMediaStream(remoteStream)
      })
    }).catch(error => {
      console.log('mediaService Answer Error:', error)
    })
  }

  useEffect(() => {
    eventManager.on('startVideoCall', startVideoCallHandler)

    peerService.on('call', callReceiveHandler)
  }, [])

  return (
        <div className="tab-pane fade show active" id="chat1" role="tabpanel">
            <div className="item">
                <div className="content">
                    <Header />
                    {mediaStream ? <VideoCall stream={mediaStream} /> : <Content />}
                    <Footer />
                </div>
            </div>
        </div>
  )
}

export default ActiveChat
