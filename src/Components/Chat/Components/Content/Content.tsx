// eslint-disable-next-line no-use-before-define
import React from 'react'
import { useSelector } from 'react-redux'

import IMessage from 'src/Contracts/IMessage'
import { selectCurrentChatMessages } from '../../../../Store/selectors/chat-selectors'
import { selectCurrentUser } from '../../../../Store/selectors/main-selectors'
import Me from './Participants/Me'
import Other from './Participants/Other'

function Content () {
  const messages: IMessage[] = useSelector(selectCurrentChatMessages)
  const me = useSelector(selectCurrentUser)

  const renderMessages = messages.map((message) => {
    return message.sender === me.hash
      ? <Me body={message.content} date={message.createdAt}/>
      : <Other body={message.content} date={message.createdAt}/>
  })

  return (
    <div className="middle" id="scroll">
        <div className="container">
            <ul>
                {renderMessages}
            </ul>
        </div>
    </div>
  )
}

export default Content
