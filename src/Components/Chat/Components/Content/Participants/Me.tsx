// eslint-disable-next-line no-use-before-define
import React from 'react'

type MeProps = {
    body: string;
    date: string;
}

function me ({ body, date }: MeProps) {
  return (
        <li className='me'>
            <div className="content">
                <div className="message">
                    <div className="bubble">
                        <p>{body}</p>
                    </div>
                </div>
                <span>
                    `${new Date(date).toLocaleDateString('fa-IR')}`
                    `${new Date(date).toLocaleTimeString('fa-IR')}`
                </span>
            </div>
        </li>
  )
}

export default me
