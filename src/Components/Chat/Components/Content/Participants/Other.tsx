// eslint-disable-next-line no-use-before-define
import React from 'react'

type OtherProps = {
    body: string;
    date: string;
}

function Other ({ body, date } : OtherProps) {
  return (
        <li className='other'>
            <div className="content">
                <div className="message">
                    <div className="bubble">
                        <p>{body}</p>
                    </div>
                </div>
                <span>
                    `${new Date(date).toLocaleDateString('fa-IR')}`
                    `${new Date(date).toLocaleTimeString('fa-IR')}`
                </span>
            </div>
        </li>
  )
}

export default Other
