// eslint-disable-next-line no-use-before-define
import React, { useState } from 'react'

import { useChatState } from '../../../../Components/Messenger/state/chat-provider'
import { ChatContext } from '../../../../Components/Messenger/state/chat-context'

function Footer () {
  const [text, setText] = useState<string>('')

  const { eventManager } = useChatState() as ChatContext

  const handleChangeTextMessage = (e:React.ChangeEvent<HTMLTextAreaElement>) => {
    const value = e.target.value.trim()

    if (!value) {
      return
    }

    setText(value)
  }

  const handleSendMessage = (e: React.MouseEvent<HTMLButtonElement>) => {
    e.preventDefault()

    if (!text) {
      return
    }

    eventManager.fire('message', {
      content: text,
      type: 'text'
    })
    setText('')
  }

  return (
    <div className="container">
        <div className="bottom">
        <form>
            <textarea
              className="form-control"
              placeholder="پیام خود را بنویسید ..."
              rows={1}
              onBlur={handleChangeTextMessage}
            />
            <button
              type="submit"
              className="btn prepend"
              onClick={handleSendMessage}
            >
              <i data-eva="paper-plane" />
            </button>
        </form>
        </div>
    </div>
  )
}

export default Footer
