// eslint-disable-next-line no-use-before-define
import React from 'react'
import { useSelector } from 'react-redux'

import { useChatState } from '../../../Messenger/state/chat-provider'
import { ChatContext } from '../../../Messenger/state/chat-context'
import { selectCurrentChatID, selectOtherParticipant } from '../../../../Store/selectors/chat-selectors'

function Header () {
  const { eventManager } = useChatState() as ChatContext

  const currentChatID = useSelector(selectCurrentChatID)
  const otherParticipant = useSelector(selectOtherParticipant)

  const { hash, fullName, avatar } = otherParticipant
  const finishCurrentChat = (e:React.MouseEvent<HTMLButtonElement>) => {
    e.preventDefault()
    eventManager.fire('finishChat', {
      chatID: currentChatID,
      to: hash
    })
  }

  const startVideoCall = (e:React.MouseEvent<HTMLButtonElement>) => {
    e.preventDefault()
    eventManager.fire('startVideoCall', {
      to: hash
    })
  }

  return (
    <div className="container">
        <div className="top">
        <div className="headline">
            <img src={avatar} alt="avatar" />
            <div className="content">
            <h5>{fullName}</h5>
            <span>دور</span>
            </div>
        </div>
        <ul>
            <li>
            <button onClick={startVideoCall} type="button" className="btn">
                <i data-eva="video" data-eva-animation="pulse" />
            </button>
            </li>
            <li>
            <button type="button" className="btn">
                <i data-eva="phone" data-eva-animation="pulse" />
            </button>
            </li>
            <li>
            <button onClick={finishCurrentChat} type="button" className="btn" data-utility="open">
                <i data-eva="close-circle-outline" data-eva-animation="pulse" />
            </button>
            </li>
            <li>
            <button type="button" className="btn round" data-chat="open">
                <i data-eva="arrow-ios-back" />
            </button>
            </li>
            <li>
            <button
                type="button"
                className="btn"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
            >
                <i data-eva="more-vertical" data-eva-animation="pulse" />
            </button>
            <div className="dropdown-menu">
                <button type="button" className="dropdown-item">
                <i data-eva="video" />
                Video call
                </button>
                <button type="button" className="dropdown-item">
                <i data-eva="phone" />
                Voice call
                </button>
                <button
                type="button"
                className="dropdown-item"
                data-toggle="modal"
                data-target="#compose"
                >
                <i data-eva="person-add" />
                Add people
                </button>
                <button type="button" className="dropdown-item" data-utility="open">
                <i data-eva="info" />
                Information
                </button>
            </div>
            </li>
        </ul>
        </div>
    </div>
  )
}

export default Header
