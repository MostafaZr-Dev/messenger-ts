// eslint-disable-next-line no-use-before-define
import React, { useRef, useEffect } from 'react'

type VideoProps = {
    stream: MediaStream | null
}

function VideoCall ({ stream }: VideoProps) {
  const video = useRef<HTMLVideoElement>(null)

  useEffect(() => {
    if (stream && video.current) {
      video.current.srcObject = stream
    }
  }, [stream])

  return (
        <div className='middle' id='scroll'>
            <div className="container">
                <video
                  id="videoPlayer"
                  ref={video}
                  style={{
                    width: '100%',
                    height: '100%',
                    backgroundColor: '#ddd'
                  }}
                  autoPlay
                  playsInline
                ></video>
            </div>
        </div>
  )
}

export default VideoCall
