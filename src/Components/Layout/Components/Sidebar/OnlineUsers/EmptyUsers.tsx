// eslint-disable-next-line no-use-before-define
import React from 'react'

function EmptyUsers () {
  return (
        <li className='alert alert-warning text-center'>
            کاربری آنلاین نیست
        </li>
  )
}

export default EmptyUsers
