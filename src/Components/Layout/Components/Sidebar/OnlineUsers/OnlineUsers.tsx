// eslint-disable-next-line no-use-before-define
import React, { useEffect } from 'react'
import { useSelector } from 'react-redux'

import IOnlineUser from 'src/Contracts/IOnlineUser'
import { selectOnlineUsers } from '../../../../../Store/selectors/main-selectors'
import EmptyUsers from './EmptyUsers'
import OnlineUsersItem from './OnlineUsersItem'

function OnlineUsers () {
  const onlineUsers = useSelector(selectOnlineUsers)

  useEffect(() => {
    window.evaReplace()
  }, [])

  const hasOnlineUsers = !!onlineUsers.length

  return (
    <div className="tab-pane fade show active" id="onlineUsers" role="tabpanel">
        <div className="top">
            <h4>کاربران آنلاین</h4>
        </div>

        <div className="middle">
          <ul className="nav discussions" role="tablist">
            {!hasOnlineUsers && <EmptyUsers />}
            {hasOnlineUsers && onlineUsers.map((onlineUser: IOnlineUser, index: number) => (
              <OnlineUsersItem key={onlineUser.user.hash} onlineUser={onlineUser} />
            ))}
          </ul>
        </div>
    </div>
  )
}

export default OnlineUsers
