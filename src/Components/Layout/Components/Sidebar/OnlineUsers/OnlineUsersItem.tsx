// eslint-disable-next-line no-use-before-define
import React from 'react'

import IOnlineUser from 'src/Contracts/IOnlineUser'
import { ChatContext } from '../../../../Messenger/state/chat-context'
import { useChatState } from '../../../../Messenger/state/chat-provider'

type OnlineUsersItemProps = {
    onlineUser: IOnlineUser
}
function OnlineUsersItem ({ onlineUser } : OnlineUsersItemProps) {
  const { eventManager } = useChatState() as ChatContext

  const sendChatRequest = () => {
    console.log('sendChatRequest')
    eventManager.fire('chatRequest', onlineUser)
  }
  return (
        <li>
            <a
                href="#chat1"
                className="filter direct active"
                data-chat="open"
                data-toggle="tab"
                role="tab"
                aria-controls="chat1"
                aria-selected="false"
                style={{}}
            >
                <div className="status online">
                <img src={onlineUser.user.avatar as string} alt="avatar" />
                <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width={24}
                    height={24}
                    viewBox="0 0 24 24"
                    className="eva eva-radio-button-on"
                >
                    <g data-name="Layer 2">
                    <g data-name="radio-button-on">
                        <rect width={24} height={24} opacity={0} />
                        <path d="M12 2a10 10 0 1 0 10 10A10 10 0 0 0 12 2zm0 18a8 8 0 1 1 8-8 8 8 0 0 1-8 8z" />
                        <path d="M12 7a5 5 0 1 0 5 5 5 5 0 0 0-5-5z" />
                    </g>
                    </g>
                </svg>
                </div>
                <div className="content">
                <div className="headline">
                    <h5>{onlineUser.user.fullName}</h5>
                    <span>آنلاین</span>
                    <span
                        onClick={sendChatRequest}
                        style={{ width: '20px', height: '20px' }}
                        data-bs-toggle="tooltip"
                        data-bs-placement="top"
                        title="درخواست گفتگو"
                    >
                        <i
                            data-eva='message-circle-outline'
                            data-eva-animation="pulse"
                        ></i>
                    </span>
                </div>
                <p>تست های بخش تولید آزمون پاس شدن</p>
                </div>
            </a>
        </li>
  )
}

export default OnlineUsersItem
