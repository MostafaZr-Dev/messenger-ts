// eslint-disable-next-line no-use-before-define
import React from 'react'

function EmptyChats () {
  return (
        <li className='alert alert-warning text-center'>
            تاکنون هیچ گفتگوی تازه ای نداشته اید
        </li>
  )
}

export default EmptyChats
