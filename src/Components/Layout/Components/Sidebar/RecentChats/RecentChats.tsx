// eslint-disable-next-line no-use-before-define
import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'

import RecentItem from './RecentItem'
import EmptyChats from './EmptyChats'
import { selectRecentChats } from '../../../../../Store/selectors/main-selectors'
import IRecentChat from '../../../../../Contracts/IRecentChat'
import { useChatState } from '../../../../Messenger/state/chat-provider'
import { ChatContext } from '../../../../Messenger/state/chat-context'
import { activeChatInit } from '../../../../../Store/actions/chat-actions'

function RecentChats () {
  const recentChats = useSelector(selectRecentChats)

  const { eventManager } = useChatState() as ChatContext

  const dispatch = useDispatch()

  const hasRecentChats = !!recentChats.length

  useEffect(() => {
    eventManager.on('activeRecentChat', (data: {chatID: string}) => {
      dispatch(activeChatInit(data.chatID))
    })
  }, [])

  const renderRecentChats = hasRecentChats
    ? recentChats.map((recentChat: IRecentChat) => <RecentItem key={recentChat.id} recentChat={recentChat}/>)
    : <EmptyChats />
  return (
        <div className="tab-pane fade" id="recentChats" role="tabpanel">
            <div className="middle">
                <h4>گفتگوهای اخیر</h4>
                <hr className='mb-3' />
                <ul className="nav discussions" role="tablist">
                    {renderRecentChats}
                </ul>
            </div>
        </div>
  )
}

export default RecentChats
