// eslint-disable-next-line no-use-before-define
import React from 'react'

import { useChatState } from '../../../../Messenger/state/chat-provider'
import { ChatContext } from '../../../../Messenger/state/chat-context'
import IRecentChat from 'src/Contracts/IRecentChat'

type RecentItemProps = {
    recentChat: IRecentChat
}

function RecentItem ({ recentChat } : RecentItemProps) {
  const { eventManager } = useChatState() as ChatContext

  const activeRecentChat = () => {
    eventManager.fire('activeRecentChat', {
      chatID: recentChat.id
    })
  }

  return (
    <li>
        <a
            onClick={activeRecentChat}
            href="#chat1"
            className="filter direct active"
            data-chat="open"
            data-toggle="tab"
            role="tab"
            aria-controls="chat1"
            aria-selected="false"
            style={{}}
        >
            <div className="status offline">
            <img src={recentChat.user?.details.avatar} alt="avatar" />
            <svg
                xmlns="http://www.w3.org/2000/svg"
                width={24}
                height={24}
                viewBox="0 0 24 24"
                className="eva eva-radio-button-on"
            >
                <g data-name="Layer 2">
                <g data-name="radio-button-on">
                    <rect width={24} height={24} opacity={0} />
                    <path d="M12 2a10 10 0 1 0 10 10A10 10 0 0 0 12 2zm0 18a8 8 0 1 1 8-8 8 8 0 0 1-8 8z" />
                    <path d="M12 7a5 5 0 1 0 5 5 5 5 0 0 0-5-5z" />
                </g>
                </g>
            </svg>
            </div>
            <div className="content">
            <div className="headline">
                <h5>{recentChat.user?.details.fullName}</h5>
                <span>{new Date(recentChat.createdAt).toLocaleDateString('fa-IR')}</span>
            </div>
            <p>{recentChat.lastMessage ? recentChat.lastMessage.content : ''}</p>
            </div>
        </a>
    </li>
  )
}

export default RecentItem
