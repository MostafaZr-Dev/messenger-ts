// eslint-disable-next-line no-use-before-define
import React from 'react'

function Settings () {
  return (
        <div className="settings tab-pane fade" id="settings" role="tabpanel">
            <div className="user">
                <label>
                <input type="file" />
                <img src="dist/img/avatars/avatar-male-1.jpg" alt="avatar" />
                </label>
                <div className="content">
                <h5>کیوان علی محمدی</h5>
                <span>ایران، کرمانشاه</span>
                </div>
            </div>
            <h4>تنظیمات</h4>
            <ul id="preferences">
                {/* Start of Account */}
                <li>
                <a
                    href="#"
                    className="headline"
                    data-toggle="collapse"
                    aria-expanded="false"
                    data-target="#account"
                    aria-controls="account"
                >
                    <div className="title">
                    <h5>حساب کاربری</h5>
                    <p>به روز رسانی پروفایل</p>
                    </div>
                    <i data-eva="arrow-ios-forward" />
                    <i data-eva="arrow-ios-downward" />
                </a>
                <div className="content collapse" id="account" data-parent="#preferences">
                    <div className="inside">
                    <form className="account">
                        <div className="form-row">
                        <div className="col-sm-6">
                            <div className="form-group">
                            <label>نام </label>
                            <input
                                type="text"
                                name="firstName"
                                id="firstName"
                                className="form-control"
                                placeholder="نام"
                                defaultValue="کیوان"
                            />
                            </div>
                        </div>
                        <div className="col-sm-6">
                            <div className="form-group">
                            <label>نام خانوادگی</label>
                            <input
                                type="text"
                                className="form-control"
                                placeholder="نام خانوادگی"
                                defaultValue="علی محمدی"
                                name="lastName"
                                id="lastName"
                            />
                            </div>
                        </div>
                        </div>
                        <div className="form-group">
                        <label>ایمیل</label>
                        <input
                            type="email"
                            className="form-control"
                            placeholder="آدرس ایمیل خود را وارد کنید"
                            name="email"
                            id="email"
                            defaultValue="keivan.amohamadi@gmail.com"
                        />
                        </div>
                        <div className="form-group">
                        <label>کلمه عبور</label>
                        <input
                            type="password"
                            className="form-control"
                            name="password"
                            id="password"
                            placeholder="کلمه عبور را وارد کنید"
                            defaultValue="*******"
                        />
                        </div>
                        <div className="form-group">
                        <label>بیو</label>
                        <textarea
                            className="form-control"
                            name="bio"
                            id="bio"
                            placeholder="درباره خودتان بنویسید"
                            defaultValue={''}
                        />
                        </div>
                        <button
                        type="submit"
                        name="saveSettings"
                        id="saveSettings"
                        className="btn primary"
                        >
                        ذخیره تنظیمات
                        </button>
                    </form>
                    </div>
                </div>
                </li>
                {/* End of Account */}
                {/* Start of Privacy & Safety */}
                <li>
                <a
                    href="#"
                    className="headline"
                    data-toggle="collapse"
                    aria-expanded="false"
                    data-target="#privacy"
                    aria-controls="privacy"
                >
                    <div className="title">
                    <h5>امنیت و حریم خصوصی</h5>
                    <p>مدیریت حریم خصوصی</p>
                    </div>
                    <i data-eva="arrow-ios-forward" />
                    <i data-eva="arrow-ios-downward" />
                </a>
                <div className="content collapse" id="privacy" data-parent="#preferences">
                    <div className="inside">
                    <ul className="options">
                        <li>
                        <div className="headline">
                            <h5>حالت امن</h5>
                            <label className="switch">
                            <input type="checkbox" defaultChecked />
                            <span className="slider round" />
                            </label>
                        </div>
                        <p>می توانید تنظیم کنید کسی آنلاین بودن شما را ببیند یا خیر</p>
                        </li>
                        <li>
                        <div className="headline">
                            <h5>تاریخچه گپ ها</h5>
                            <label className="switch">
                            <input type="checkbox" defaultChecked />
                            <span className="slider round" />
                            </label>
                        </div>
                        <p>می توانید تنظیم کنید تاریخچه ها تا چه زمانی در دسترس باشند.</p>
                        </li>
                        <li>
                        <div className="headline">
                            <h5>دسترسی به دوربین</h5>
                            <label className="switch">
                            <input type="checkbox" />
                            <span className="slider round" />
                            </label>
                        </div>
                        <p />
                        </li>
                        <li>
                        <div className="headline">
                            <h5>دسترسی به میکرفون</h5>
                            <label className="switch">
                            <input type="checkbox" />
                            <span className="slider round" />
                            </label>
                        </div>
                        <p />
                        </li>
                    </ul>
                    </div>
                </div>
                </li>
                {/* End of Privacy & Safety */}
                {/* Start of Notifications */}
                <li>
                <a
                    href="#"
                    className="headline"
                    data-toggle="collapse"
                    aria-expanded="false"
                    data-target="#alerts"
                    aria-controls="alerts"
                >
                    <div className="title">
                    <h5>اعلانات</h5>
                    <p>روش یا خاموش کردن اعلانات</p>
                    </div>
                    <i data-eva="arrow-ios-forward" />
                    <i data-eva="arrow-ios-downward" />
                </a>
                <div className="content collapse" id="alerts" data-parent="#preferences">
                    <div className="inside">
                    <ul className="options">
                        <li>
                        <div className="headline">
                            <h5>پیام های روی صفحه</h5>
                            <label className="switch">
                            <input type="checkbox" defaultChecked />
                            <span className="slider round" />
                            </label>
                        </div>
                        <p>می توانید تنظیم کنید پیام ها روی صفحه ظاهر شوند یا خیر/</p>
                        </li>
                        <li>
                        <div className="headline">
                            <h5>لرزش صفحه</h5>
                            <label className="switch">
                            <input type="checkbox" />
                            <span className="slider round" />
                            </label>
                        </div>
                        <p>
                            می توانید تعیین کنید در هنگام پیام صفحه لرزش داشته باشد یا خیر
                        </p>
                        </li>
                        <li>
                        <div className="headline">
                            <h5>صدا</h5>
                            <label className="switch">
                            <input type="checkbox" defaultChecked />
                            <span className="slider round" />
                            </label>
                        </div>
                        <p>در هنگام پیام جدید،صدا پخش شود یا خیر</p>
                        </li>
                        <li>
                        <div className="headline">
                            <h5>نور</h5>
                            <label className="switch">
                            <input type="checkbox" />
                            <span className="slider round" />
                            </label>
                        </div>
                        <p>نور صفحه در هنگام پیام جدید به چه صورت باشد</p>
                        </li>
                    </ul>
                    </div>
                </div>
                </li>
                {/* End of Notifications */}
                {/* Start of Integrations */}
                <li>
                <a
                    href="#"
                    className="headline"
                    data-toggle="collapse"
                    aria-expanded="false"
                    data-target="#integrations"
                    aria-controls="integrations"
                >
                    <div className="title">
                    <h5>ادغام سازی</h5>
                    <p>استفاده از شبکه های اجتماعی</p>
                    </div>
                    <i data-eva="arrow-ios-forward" />
                    <i data-eva="arrow-ios-downward" />
                </a>
                <div
                    className="content collapse"
                    id="integrations"
                    data-parent="#preferences"
                >
                    <div className="inside">
                    <ul className="integrations">
                        <li>
                        <button className="btn round">
                            <i data-eva="google" />
                        </button>
                        <div className="content">
                            <div className="headline">
                            <h5>گوگل</h5>
                            <label className="switch">
                                <input type="checkbox" defaultChecked />
                                <span className="slider round" />
                            </label>
                            </div>
                            <span>خواندن، نوشتن،ویرایش</span>
                        </div>
                        </li>
                        <li>
                        <button className="btn round">
                            <i data-eva="facebook" />
                        </button>
                        <div className="content">
                            <div className="headline">
                            <h5>فیس بوک</h5>
                            <label className="switch">
                                <input type="checkbox" defaultChecked />
                                <span className="slider round" />
                            </label>
                            </div>
                            <span>خواندن،نوشتن،ویرایش</span>
                        </div>
                        </li>
                        <li>
                        <button className="btn round">
                            <i data-eva="twitter" />
                        </button>
                        <div className="content">
                            <div className="headline">
                            <h5>توئیتر</h5>
                            <label className="switch">
                                <input type="checkbox" />
                                <span className="slider round" />
                            </label>
                            </div>
                            <span>بدون دسترسی</span>
                        </div>
                        </li>
                        <li>
                        <button className="btn round">
                            <i data-eva="linkedin" />
                        </button>
                        <div className="content">
                            <div className="headline">
                            <h5>لینکد این</h5>
                            <label className="switch">
                                <input type="checkbox" />
                                <span className="slider round" />
                            </label>
                            </div>
                            <span>بدون دسترسی</span>
                        </div>
                        </li>
                    </ul>
                    </div>
                </div>
                </li>
                {/* End of Integrations */}
                {/* Start of Appearance */}
                <li>
                <a
                    href="#"
                    className="headline"
                    data-toggle="collapse"
                    aria-expanded="false"
                    data-target="#appearance"
                    aria-controls="appearance"
                >
                    <div className="title">
                    <h5>ظاهر برنامه</h5>
                    <p>سفارشی سازی ظاهر برنامه</p>
                    </div>
                    <i data-eva="arrow-ios-forward" />
                    <i data-eva="arrow-ios-downward" />
                </a>
                <div
                    className="content collapse"
                    id="appearance"
                    data-parent="#preferences"
                >
                    <div className="inside">
                    <ul className="options">
                        <li>
                        <div className="headline">
                            <h5>حالت شب</h5>
                            <label className="switch">
                            <input type="checkbox" />
                            <span className="slider round mode" />
                            </label>
                        </div>
                        <p>
                            با استفاده از این مورد می توانید حالت شب را فعال یا غیر فعال
                            کنید
                        </p>
                        </li>
                    </ul>
                    </div>
                </div>
                </li>
                {/* End of Appearance */}
            </ul>
        </div>

  )
}

export default Settings
