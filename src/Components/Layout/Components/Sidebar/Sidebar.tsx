// eslint-disable-next-line no-use-before-define
import React from 'react'

import OnlineUsers from './OnlineUsers'
import RecentChats from './RecentChats'
import Notifications from './Notifications'
import Settings from './Settings'

function Sidebar () {
  return (
    <div className="sidebar">
        <div className="container">
            <div className="tab-content">
                <OnlineUsers />
                <RecentChats />
                <Notifications />
                <Settings />
            </div>
        </div>
    </div>
  )
}

export default Sidebar
