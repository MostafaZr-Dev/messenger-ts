// eslint-disable-next-line no-use-before-define
import React, { useEffect } from 'react'

import Navigation from './Components/Navigation'
import Sidebar from './Components/Sidebar'

function Layout ({ children }: React.PropsWithChildren<{}>) {
  useEffect(() => {
    window.evaReplace()
  }, [])

  return (
        <div className='layout'>
            <Navigation />
            <Sidebar />
            {children}
        </div>
  )
}

export default Layout
