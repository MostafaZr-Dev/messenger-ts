/* eslint-disable no-unused-vars */
// eslint-disable-next-line no-use-before-define
import React, { useEffect, useMemo, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import IOnlineUser from '../../Contracts/IOnlineUser'
import Layout from '../Layout'
import Chat from '../Chat'
import NearBy from '../NearBy'
import { ChatContext } from './state/chat-context'
import { useChatState } from './state/chat-provider'
import ConversationContext from './state/conversation-context'
import { selectCurrentChat } from '../../Store/selectors/chat-selectors'
import { updateOnlineUsers } from '../../Store/actions/main-actions'
import { selectCurrentUser } from '../../Store/selectors/main-selectors'
import { initNewChat } from '../../Store/actions/chat-actions'
import useNotification from '../../Hooks/useNotification'
import { DataConnection } from 'src/Contracts/Peer'

function Messenger () {
  const [requestSender, setRequestSender] = useState<DataConnection | null>(null)
  const [requestReceiver, setRequestReceiver] = useState<DataConnection | null>(null)

  const {
    peerService,
    eventManager,
    socketService
  } = useChatState() as ChatContext

  const currentChat = useSelector(selectCurrentChat)
  const me = useSelector(selectCurrentUser)
  const dispatch = useDispatch()

  const notify = useNotification()

  const setOnlineUsers = (data: {onlineUsers: IOnlineUser[]}) => {
    console.log({ data })
    dispatch(updateOnlineUsers(data.onlineUsers))
  }

  const newChatRequest = (data: {requestor: string}) => {
    notify({
      title: 'درخواست گفتگوی جدید',
      text: 'یک درخواست گفتگوی جدید دارید؟',
      icon: 'warning',
      buttons: {
        cancel: {
          text: 'رد',
          value: false,
          closeModal: true,
          visible: true
        },
        confirm: {
          text: 'قبول',
          value: true,
          closeModal: true
        }
      }
    }).then((value:any) => {
      if (value) {
        socketService.$emit('newChatResponse', {
          to: data.requestor,
          sender: me.hash,
          answer: true
        })
      } else {
        socketService.$emit('newChatResponse', {
          to: data.requestor,
          sender: me.hash,
          answer: false
        })
      }
    })
  }

  const newChatResponse = (data: {sender: string; answer:boolean}) => {
    if (data.answer) {
      console.log('قبول', { sender: data.sender })
      const connection = peerService.connect(data.sender)
      console.log({ peer: connection })
      setRequestSender(connection)
      dispatch(initNewChat(data.sender))
    } else {
      notify({
        title: 'نتیجه درخواست',
        text: 'درخواست شما پذیرفته نشد!',
        icon: 'error',
        buttons: {
          confirm: {
            text: 'باشه',
            value: true,
            closeModal: true
          }
        }
      })
    }
  }

  const peerConnectionHandler = (connection: DataConnection) => {
    console.log({ peer: connection.peer })
    setRequestReceiver(connection)
    dispatch(initNewChat(connection.peer))
  }

  const chatRequestHandler = (onlineUser: IOnlineUser) => {
    notify({
      title: 'درخواست گفتگو',
      text: 'در صورت تایید به صفحه گفتگو هدایت خواهید شد',
      icon: 'success',
      buttons: {
        confirm: {
          text: 'باشه',
          value: true,
          closeModal: true
        }
      }
    })

    socketService.$emit('newChatRequest', {
      to: onlineUser.user.hash,
      requestor: me.hash
    })
  }

  useEffect(() => {
    socketService.$on('onlineUsers', setOnlineUsers)

    socketService.$on('newChatRequest', newChatRequest)

    socketService.$on('newChatResponse', newChatResponse)

    peerService.onConnection(peerConnectionHandler)

    if (eventManager) {
      eventManager.on('chatRequest', chatRequestHandler)
    }

    return () => {
      socketService.$off('onlineUsers', setOnlineUsers)
      socketService.$off('newChatRequest', newChatRequest)
      socketService.$off('newChatResponse', newChatResponse)

      peerService.offConnectionHandler(peerConnectionHandler)
    }
  }, [])

  const currentScene = currentChat ? <Chat /> : <NearBy />

  return (
    <ConversationContext.Provider value={{
      sender: requestSender,
      receiver: requestReceiver
    }}>
      <Layout>
        {currentScene}
      </Layout>
    </ConversationContext.Provider>
  )
}

export default Messenger
