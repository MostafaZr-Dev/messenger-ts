import { createContext } from 'react'

import PeerService from '../../../Services/PeerService'
import EventManager from '../../../EventManager'
import SocketClient from '../../../Services/SocketClient'

export type ChatContext = {
    peerService: PeerService
    eventManager: EventManager,
    socketService: SocketClient
}

export default createContext<ChatContext | {}>({})
