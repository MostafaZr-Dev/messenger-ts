// eslint-disable-next-line no-use-before-define
import React, { useContext, useMemo } from 'react'
import { useSelector } from 'react-redux'

import ChatContext from './chat-context'
import PeerService from '../../../Services/PeerService'
import ClientService from '../../../Services/ClientService'
import SocketService from '../../../Services/SocketClient'
import { selectCurrentUser } from '../../../Store/selectors/main-selectors'
import EventManager from '../../../EventManager'

export const ChatProvider = ({ children }: React.PropsWithChildren<{}>) => {
  const clientService = useMemo(() => new ClientService(), [])
  const peerService = useMemo(() => new PeerService(), [])
  const socketService = useMemo(() => new SocketService(), [])

  const me = useSelector(selectCurrentUser)

  const buildChatContextData = (): object => {
    if (me) {
      clientService.setCurrentClient(me)
      socketService.setClient(clientService)
      socketService.connect()
      peerService.setClient(clientService)
      peerService.init()

      return {
        eventManager: new EventManager(),
        peerService,
        socketService
      }
    }

    return {}
  }

  return (
    <ChatContext.Provider value={buildChatContextData()}>
        {children}
    </ChatContext.Provider>
  )
}

export const useChatState = () => {
  return useContext(ChatContext)
}
