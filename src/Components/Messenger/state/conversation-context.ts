import { createContext } from 'react'
import { DataConnection } from 'src/Contracts/Peer'

export type ConversationContext = {
    sender: DataConnection | null
    receiver: DataConnection | null,
}

export default createContext<ConversationContext | {}>({})
