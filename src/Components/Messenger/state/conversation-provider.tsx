import { useContext } from 'react'

import ConversationContext from './conversation-context'

export const useConversationContext = () => {
  return useContext(ConversationContext)
}
