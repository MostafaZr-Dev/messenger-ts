// eslint-disable-next-line no-use-before-define
import React, { useLayoutEffect } from 'react'
import * as leaflet from 'leaflet'

import IOnlineUser from '../../../../Contracts/IOnlineUser'
import IUser from 'src/Contracts/IUser'

type MapProps = {
    current: {
        lat: number;
        lng: number;
    };
    zoom: number | undefined;
    onlineUsers: IOnlineUser[];
    me: IUser
}

function Map ({ current, zoom, onlineUsers, me }: MapProps) {
  useLayoutEffect(() => {
    const { lat, lng } = current

    const map = leaflet.map('map').setView([lat, lng], zoom)

    leaflet.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map)

    if (me) {
      leaflet.marker([lat, lng]).addTo(map).bindPopup(`<p class='map__popup'>
        <img src='${me.avatar}' alt='user-avatar' />
        <span>من</span>
      </p>`, {
        className: 'online-users-popup'
      })

      const distanceMarker = new leaflet.Circle([lat, lng], {
        radius: 500,
        color: 'yellow'
      })
      distanceMarker.addTo(map)
    }

    onlineUsers.forEach(onlineUser => {
      const [lat, lng] = onlineUser.location.coordinates
      leaflet.marker([lat, lng]).addTo(map).bindPopup(`<p class='map__popup'>
        <img src='${onlineUser.user.avatar}' alt='user-avatar' />
        <span>${onlineUser.user.fullName}</span>
      </p>`, {
        className: 'online-users-popup'
      })
    })

    return () => {
      map.remove()
    }
  }, [onlineUsers])

  return (
        <div id='map'>

        </div>
  )
}

export default Map
