// eslint-disable-next-line no-use-before-define
import React from 'react'
import { useSelector } from 'react-redux'

import Map from './Components/Map'
import { selectCurrentUser, selectOnlineUsers } from '../../Store/selectors/main-selectors'

function NearBy () {
  const onlineUsers = useSelector(selectOnlineUsers)
  const me = useSelector(selectCurrentUser)

  return (
        <div className='chat'>
            <div className="tab-content">
                <div className="map__view">
                    <Map
                        current={{
                          lat: me.location.latitude,
                          lng: me.location.longitude
                        }}
                        me={me}
                        zoom={15}
                        onlineUsers={onlineUsers}
                    />
                </div>
            </div>
        </div>
  )
}

export default NearBy
