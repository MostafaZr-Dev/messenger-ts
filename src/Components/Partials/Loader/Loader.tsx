// eslint-disable-next-line no-use-before-define
import React from 'react'

function Loader () {
  return (
        <div className='loader'>
            <div className='loader__content'>
                <img className='loader__content__img' src="/assets/img/favicon.png" />
            </div>
        </div>
  )
}

export default Loader
