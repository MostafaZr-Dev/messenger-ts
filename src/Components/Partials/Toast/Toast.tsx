// eslint-disable-next-line no-use-before-define
import React, { useEffect } from 'react'
import { ToastContainer, toast } from 'react-toastify'

type ToastProps = {
    type: 'success' | 'error' | 'warning';
    message: string;
    position?: 'top-center' | 'bottom-center';
    delay?: number;
    show: boolean
}

function Toast ({
  show = false,
  type = 'success',
  message,
  position = 'top-center',
  delay = 5000
} : ToastProps
) {
  useEffect(() => {
    if (show) {
      toast[type](message)
    }
  }, [show])

  return (
    <ToastContainer
        position={position}
        autoClose={delay}
        hideProgressBar
        newestOnTop={false}
        closeOnClick
        rtl
        pauseOnFocusLoss
        pauseOnHover
    />
  )
}

export default Toast
