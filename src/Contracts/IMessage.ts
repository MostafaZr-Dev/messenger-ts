export default interface IMessage {

    sender: string;
    type: 'text' | 'file';
    content: string;
    createdAt: string;
}
