import IUser from './IUser'

export default interface IOnlineUser {

    user: IUser;
    location: {
        coordinates: [
            number,
            number
        ]
    }

}
