import IMessage from './IMessage'

type Participant = {
    hash: string;
    details: {
        hash: string;
        fullName: string;
        avatar: string;
    }
}

export default interface IRecentChat {
    id: string;
    user?: Participant;
    lastMessage?: IMessage | null;
    createdAt: string;
}
