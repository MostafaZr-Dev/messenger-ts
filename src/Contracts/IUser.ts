
export type LocationType = {
    latitude: number;
    longitude: number;
}

export default interface IUser {
    hash: string;
    fullName: string;
    email: string;
    location: LocationType | null,
    address: string | null,
    avatar: string | null
}
