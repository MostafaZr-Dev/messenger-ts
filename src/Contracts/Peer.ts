import Peer from 'peerjs'

export type PeerObject = Peer;
export type DataConnection = Peer.DataConnection
export type MediaConnection = Peer.MediaConnection
