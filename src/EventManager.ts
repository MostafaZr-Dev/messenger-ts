
type Handler = (...data: any[]) => void
export default class EventManager {
    private readonly events: Map<string, Handler>

    constructor () {
      // eslint-disable-next-line func-call-spacing
      this.events = new Map<string, Handler>()
    }

    public on (event: string, handler: Handler) {
      this.events.set(event, handler)
    }

    public fire (event: string, ...data:any[]) {
      const handler = this.events.get(event)

      if (!handler) {
        throw new Error(`${event} does not exist!`)
      }

      handler(...data)
    }
}
