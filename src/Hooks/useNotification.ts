import { useCallback } from 'react'
import swal from 'sweetalert'

type buttonType = {
    [key: string] : {
        text: string | undefined,
        value: any,
        visible?: boolean,
        className?: string | undefined,
        closeModal: boolean,
    }
}

type NotifyParams = {
    title: string | undefined;
    text: string | undefined;
    icon: 'success' | 'error' | 'warning' | undefined;
    buttons?: buttonType
}

const useNotification = () => {
  const notify = useCallback(
    (config: NotifyParams) => {
      return swal({
        title: config.title,
        text: config.text,
        icon: config.icon,
        buttons: config.buttons
      })
    },
    []
  )

  return notify
}

export default useNotification
