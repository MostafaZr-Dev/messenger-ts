// eslint-disable-next-line no-use-before-define
import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Link, Redirect } from 'react-router-dom'

import { userLogin } from '../../../Store/actions/user-actions'
import { selectApiState } from '../../../Store/selectors/user-selectors'
import { selectLogginFlag } from '../../../Store/selectors/main-selectors'
import Toast from '../../../Components/Partials/Toast'

function Login () {
  const [userInfo, setUserInfo] = useState<{email:string;password:string;}>({
    email: '',
    password: ''
  })

  const dispatch = useDispatch()
  const { success, error, message } = useSelector(selectApiState)
  const isUserLoggedIn = useSelector(selectLogginFlag)

  useEffect(() => {
    window.evaReplace()
  }, [])

  useEffect(() => {
    let timeout:ReturnType<typeof setTimeout>

    if (success) {
      timeout = setTimeout(() => {
        location.replace('/app')
      }, 3000)
    }

    return () => {
      clearTimeout(timeout)
    }
  }, [success])

  if (isUserLoggedIn) {
    return <Redirect to='/app' />
  }

  const handleChange = (e:React.ChangeEvent<HTMLInputElement>) => {
    const value = e.target.value.trim()

    if (!value) {
      return
    }

    setUserInfo(prevValue => ({
      ...prevValue,
      [e.target.name]: value
    }))
  }

  const doLogin = (e:React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()

    if (Object.values(userInfo).includes('')) {
      return
    }

    dispatch(userLogin(userInfo.email, userInfo.password))
  }

  return (
    <div className="sign">
        <div className="container">
          <Toast show={success} type='success' message={message} />
          <Toast show={error} type='error' message={message} delay={3000} />
          <div className="item">
              <form onSubmit={doLogin}>
                  <h2>ورود</h2>
                  <div className="form-group">
                      <input
                      name="email"
                      type="email"
                      className="form-control"
                      placeholder="ایمیل"
                      onChange={handleChange}
                      required
                      autoFocus
                      />
                      <button className="btn prepend">
                      <i data-eva="person" />
                      </button>
                  </div>
                  <div className="form-group">
                      <input
                      type="password"
                      name="password"
                      className="form-control"
                      placeholder="کلمه عبور"
                      onChange={handleChange}
                      required
                      />
                      <button className="btn prepend">
                      <i data-eva="lock" />
                      </button>
                  </div>
                  <a href="#">فراموشی کلمه عبور؟</a>
                  <button type="submit" className="btn primary">
                      ورود
                  </button>
                  <span>
                      حساب کاربری ندارید؟ <Link to='/auth/register'>ثبت نام</Link>
                  </span>
              </form>
          </div>
        </div>
    </div>
  )
}

export default Login
