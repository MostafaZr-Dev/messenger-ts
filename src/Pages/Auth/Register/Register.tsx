// eslint-disable-next-line no-use-before-define
import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Link, Redirect } from 'react-router-dom'

import { userRegister } from '../../../Store/actions/user-actions'
import { selectApiState } from '../../../Store/selectors/user-selectors'
import { selectLogginFlag } from '../../../Store/selectors/main-selectors'
import Toast from '../../../Components/Partials/Toast'

function Register () {
  const [userInfo, setUserInfo] = useState<{fullName: string;email:string;password:string;}>({
    fullName: '',
    email: '',
    password: ''
  })

  const dispatch = useDispatch()
  const { success, message } = useSelector(selectApiState)
  const isUserLoggedIn = useSelector(selectLogginFlag)

  useEffect(() => {
    window.evaReplace()
  }, [])

  useEffect(() => {
    let timeout:ReturnType<typeof setTimeout>

    if (success) {
      timeout = setTimeout(() => {
        location.replace('/auth/login')
      }, 3000)
    }

    return () => {
      clearTimeout(timeout)
    }
  }, [success])

  if (isUserLoggedIn) {
    return <Redirect to='/app' />
  }

  const handleChange = (e:React.ChangeEvent<HTMLInputElement>) => {
    const value = e.target.value.trim()

    if (!value) {
      return
    }

    setUserInfo(prevValue => ({
      ...prevValue,
      [e.target.name]: value
    }))
  }

  const handleUserRegister = (e:React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()

    if (Object.values(userInfo).includes('')) {
      return
    }

    dispatch(userRegister(
      userInfo.fullName,
      userInfo.email,
      userInfo.password
    ))
  }

  return (
        <div className="sign">
            <div className="container">
                <div className="item">
                <Toast show={success} type='success' message={message} />
                <form onSubmit={handleUserRegister}>
                    <h2>ثبت نام</h2>
                    <div className="form-group">
                    <input
                        type="text"
                        name="fullName"
                        className="form-control"
                        placeholder="نام و نام خانوادگی مانند مصطفی زارعی"
                        onChange={handleChange}
                        required
                        autoFocus
                    />
                    <button className="btn prepend">
                        <i data-eva="person" />
                    </button>
                    </div>
                    <div className="form-group">
                    <input
                        type="email"
                        name="email"
                        className="form-control"
                        placeholder="ایمیل مانند mostafa@gmail.com"
                        onChange={handleChange}
                        required
                    />
                    <button className="btn prepend">
                        <i data-eva="email" />
                    </button>
                    </div>
                    <div className="form-group">
                    <input
                        type="password"
                        name="password"
                        className="form-control"
                        placeholder="رمز عبور..."
                        onChange={handleChange}
                        required
                    />
                    <button className="btn prepend">
                        <i data-eva="lock" />
                    </button>
                    </div>
                    <button type="submit" className="btn primary">
                    ثبت نام
                    </button>
                    <span>
                    حساب کاربری دارید؟ <Link to='/auth/login'>ورود</Link>
                    </span>
                </form>
                </div>
            </div>
    </div>
  )
}

export default Register
