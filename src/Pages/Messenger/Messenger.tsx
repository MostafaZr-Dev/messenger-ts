// eslint-disable-next-line no-use-before-define
import React from 'react'
import { useSelector } from 'react-redux'
import { Redirect } from 'react-router-dom'

import Messenger from '../../Components/Messenger'
import { selectLogginFlag, selectUserLocation } from '../../Store/selectors/main-selectors'
import { ChatProvider } from '../../Components/Messenger/state/chat-provider'
import Address from '../../Components/Address'

function MessengerPage () {
  const isUserLoggedIn = useSelector(selectLogginFlag)
  const userLocation = useSelector(selectUserLocation)

  if (!isUserLoggedIn) {
    return <Redirect to='/auth/login' />
  }

  if (!userLocation) {
    return <Address />
  }

  return (
    <ChatProvider>
      <Messenger />
    </ChatProvider>
  )
}

export default MessengerPage
