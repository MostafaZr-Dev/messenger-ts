// eslint-disable-next-line no-use-before-define
import React from 'react'

import routes from './routes'
import RouteWithSubRoutes from './RouteWithSubRoutes'

function AppRoutes () {
  const renderRoutes = routes.map((route, index) => (
    <RouteWithSubRoutes key={index} route={route} />
  ))

  return (
    <>
      {renderRoutes}
    </>
  )
}

export default AppRoutes
