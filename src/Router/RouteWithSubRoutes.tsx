// eslint-disable-next-line no-use-before-define
import React from 'react'
import { Route } from 'react-router-dom'

interface RouteItem {
    path: string;
    component: any;
    exact: boolean;
    routes: RouteItem[];
}

type RouteWithSubRoutesProps = {
    key: React.Key;
    base?: string;
    route: RouteItem;
}

function RouteWithSubRoutes ({ base, route }:RouteWithSubRoutesProps) {
  if (base) {
    const path = `${base}${route.path}`
    return (
      <Route key={path} exact={route.exact} path={path}>
        <route.component base={path} routes={route.routes} />
      </Route>
    )
  }

  return (
    <Route key={route.path} exact={route.exact} path={route.path}>
      <route.component base={route.path} routes={route.routes} />
    </Route>
  )
}

export default RouteWithSubRoutes
