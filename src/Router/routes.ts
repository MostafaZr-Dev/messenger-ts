import Messenger from '../Pages/Messenger'
import Login from '../Pages/Auth/Login'
import Register from '../Pages/Auth/Register'

interface RouteItem {

    path: string;
    component: any;
    exact: boolean;
    routes: RouteItem[];

}

const routes: RouteItem[] = [

  {
    path: '/app',
    component: Messenger,
    exact: false,
    routes: []
  },
  {
    path: '/auth/login',
    component: Login,
    exact: false,
    routes: []
  },
  {
    path: '/auth/register',
    component: Register,
    exact: false,
    routes: []
  }
]

export default routes
