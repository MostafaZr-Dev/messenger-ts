import httpService from './HttpService'
import IUser from '../Contracts/IUser'
import IRecentChat from 'src/Contracts/IRecentChat'

type AuthResponse = {
  success: true;
  user?: IUser;
  recentChats: IRecentChat[]
}
type RegisterResponse = {
    success: true;
    message: string;
}
type LoginResponse = {
  success: true;
  message: string;
  token?: string;
}
export default class AuthService {
  public async authenticate (token: string) {
    return await httpService.get<AuthResponse>('/api/v1/auth/init', {
      headers: {
        authorization: `Bearer ${token}`
      }
    })
  }

  public async register (fullName:string, email:string, password: string) {
    return await httpService.post<RegisterResponse, {fullName:string; email:string; password: string}>('/api/v1/auth/register', {
      fullName,
      email,
      password
    })
  }

  public async login (email:string, password: string) {
    return await httpService.post<LoginResponse, {email:string; password: string}>('/api/v1/auth/login', {
      email,
      password
    })
  }
}
