import httpService from './HttpService'

type NewChatRespons = {
    success: boolean;
    chat: {
      id: string;
      participants : object;
    }
}

type SaveMessageRespons = {
  success: boolean;

}

type FinishChatRespons = {
  success: boolean;

}

type ActiveChatResponse = {
  success: boolean;
  chat: {
    currentChat: object;
    currentChatMessages: [];
  }
}

export default class ChatService {
  public async newChat (token: string, participant: object) {
    return httpService.post<NewChatRespons, {participant: object}>('/api/v1/chat', {
      participant
    }, {
      headers: {
        authorization: `Bearer ${token}`
      }
    })
  }

  public async saveMessage (token: string, data: object) {
    return httpService.post<SaveMessageRespons, {}>('/api/v1/chat/messages', {
      ...data
    }, {
      headers: {
        authorization: `Bearer ${token}`
      }
    })
  }

  public async finishChat (token:string, chatID: string) {
    return httpService.patch<FinishChatRespons, {}>(`/api/v1/chat/${chatID}/finish`, {}, {
      headers: {
        authorization: `Bearer ${token}`
      }
    })
  }

  public async fetchActiveChat (token:string, chatID: string) {
    return httpService.get<ActiveChatResponse>(`/api/v1/chat/${chatID}/active`, {
      headers: {
        authorization: `Bearer ${token}`
      }
    })
  }
}
