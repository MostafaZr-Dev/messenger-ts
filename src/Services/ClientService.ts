import IUser, { LocationType } from 'src/Contracts/IUser'
import httpService from './HttpService'

type SetLocationResponse = {
  success: true;
}

type SetAddressResponse = {
  location: {
    x: number,
    y: number
  },
  status: 'OK'
}

export default class ClientService {
  private me:IUser | null = null

  public setCurrentClient (me: IUser) {
    this.me = me
  }

  public getClientID (): string | null {
    return this.me ? this.me.hash : null
  }

  public getClientLocation () : LocationType | null {
    return this.me?.location ? this.me.location : null
  }

  public getClientAddress () : string | null {
    return this.me?.address ? this.me.address : null
  }

  public async setLocation (token:string, address:string, latitude: number, longitude:number) {
    return await httpService.post<SetLocationResponse, {address:string;latitude: number; longitude:number}>(
      '/api/v1/me/location',
      {
        latitude,
        longitude,
        address
      },
      {
        headers: {
          authorization: `Bearer ${token}`
        }
      }
    )
  }

  public async setAddress (address:string) {
    return await httpService.get<SetAddressResponse>(
      `https://api.neshan.org/v4/geocoding?address=${address}`,
      {
        headers: {
          'Api-Key': 'service.8wb8pcWhf07ZXvBQEe1rUYJJes5FnByHmBLIfRAf'
        }
      }
    )
  }
}
