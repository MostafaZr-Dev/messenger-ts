
export default class LocationService {
  public getUserLocation (): Promise<any> {
    if (!this.isLocationSupported) {
      throw new Error('Location in this browser does not supported!')
    }

    const promise = new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition((position) => {
        resolve(position.coords)
      }, (error) => {
        reject(error)
      })
    })

    return promise
  }

  private isLocationSupported ():boolean {
    return ('geolocation' in navigator)
  }
}
