
type MediaConstraints = {
    video?: boolean;
    audio?: boolean;
}

export default class MediaService {
  public async requestMediaPermission (options: MediaConstraints) {
    try {
      const stream: MediaStream = await navigator.mediaDevices.getUserMedia(options)

      return stream
    } catch (error) {
      return error
    }
  }
}
