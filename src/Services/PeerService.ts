import Peer from 'peerjs'

import ClientService from './ClientService'
import { PeerObject, DataConnection, MediaConnection } from 'src/Contracts/Peer'

export default class PeerService {
    private client: ClientService | null = null
    private peer: PeerObject | null = null
    private connection: DataConnection | null

    public constructor () {
      this.connection = null
    }

    public setClient (client:ClientService): void {
      this.client = client
    }

    public init (): void {
      if (!this.client) {
        throw new Error('client does not set!')
      }

      this.peer = new Peer(this.client.getClientID() as string, {
        port: 9000,
        key: 'messenger',
        path: '/messenger',
        host: '/'
      })

      this.peer.on('disconnected', () => {
        this.peer?.reconnect()

        console.log(this.peer)
      })
    }

    public onConnection (handler:(connection: DataConnection) => void) {
      this.peer?.on('connection', handler)
    }

    public offConnectionHandler (handler: Function) {
      this.peer?.off('connection', handler)
    }

    public connect (clientID: string): DataConnection {
      if (!this.peer) {
        throw new Error('peer does not set!')
      }

      console.log('peer:', this.peer.disconnected)
      this.connection = this.peer.connect(clientID)
      console.log(this.connection)
      return this.connection
    }

    public on (event: string, handler: any): void {
      if (!this.connection) {
        throw new Error('connection not exist!')
      }

      this.connection.on(event, handler)
    }

    public call (clientID: string, stream: MediaStream): MediaConnection {
      if (!this.peer) {
        throw new Error('peer does not set!')
      }

      return this.peer.call(clientID, stream)
    }
}
