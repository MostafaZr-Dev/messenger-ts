import { io, Socket } from 'socket.io-client'

import ClientService from './ClientService'

type Handler = (...data: any[]) => void
export default class SocketClient {
    private client: ClientService | null
    private connection: Socket | null
    private readonly events: Map<string, Handler> = new Map<string, Handler>()

    constructor () {
      this.client = null
      this.connection = null
    }

    public setClient (client: ClientService) {
      this.client = client
    }

    public connect () {
      this.connection = io('https://localhost:8000', {
        path: '/messenger',
        query: {
          hash: this.client?.getClientID() as string,
          lat: `${this.client?.getClientLocation()?.latitude}`,
          long: `${this.client?.getClientLocation()?.longitude}`,
          address: this.client?.getClientAddress() as string
        }
      })
      this.connection.on('connect', this.connectionHandler.bind(this))
    }

    public connectionHandler () {
      console.log(`client connected with ID: ${this.connection?.id}`)
    }

    public $on (event:string, handler: (...data: any[]) => void) {
      if (!this.connection) {
        throw new Error('connection does not set!')
      }

      this.connection.on(event, handler)
    }

    public $emit (event:string, ...data: any[]) {
      if (!this.connection) {
        throw new Error('connection does not set!')
      }

      this.connection.emit(event, ...data)
    }

    public $off (event: string, handler: Function) {
      if (!this.connection) {
        throw new Error('connection does not set!')
      }

      this.connection.off(event)
    }
}
