export const userActionTypes = {
  API_INIT: 'users/apiInit',
  USER_REGISTER: 'users/register',
  USER_REGISTER_SUCCESS: 'users/registerSuccess',
  USER_REGISTER_FAILED: 'users/registerFailed',
  USER_LOGIN: 'users/login',
  USER_LOGIN_SUCCESS: 'user/loginSuccess',
  USER_LOGIN_FAILED: 'user/loginFailed',
  USER_SET_LOCATION: 'user/setLocation',
  USER_SET_LOCATION_SUCCESS: 'user/setLocationSuccess',
  USER_SET_LOCATION_FAILED: 'user/setLocationFailed'
}

export const mainActionTypes = {
  API_ISLOADING: 'main/API_ISLOADING',
  AUTH_USER: 'main/authUser',
  AUTH_USER_SUCCESS: 'main/authUserSuccess',
  AUTH_USER_FAILED: 'main/authUserFailed',
  USER_SET_ADDRESS: 'user/setAddress',
  USER_SET_ADDRESS_SUCCESS: 'user/setAddressSuccess',
  USER_SET_ADDRESS_FAILED: 'user/setAddressFailed',
  UPDATE_ONLINE_USERS: 'main/updateOnlineUsers',
  UPDATE_ONLINE_USERS_SUCCESS: 'main/updateOnlineUsersSuccess',
  LOGOUT_REQUEST: 'chat/logoutRequest',
  LOGOUT_REQUEST_SUCCESS: 'chat/logoutRequestSuccess',
  LOGOUT_REQUEST_FAILED: 'chat/logoutRequestFailed'
}

export const chatActionTypes = {
  INIT_NEW_CHAT: 'chat/initNewChat',
  INIT_NEW_CHAT_SUCCESS: 'chat/initNewChatSuccess',
  INIT_NEW_CHAT_FAILED: 'chat/initNewChatFailed',
  SEND_MESSAGE_INIT: 'chat/sendMessageInit',
  SEND_MESSAGE_INIT_SUCCESS: 'chat/sendMessageInitSuccess',
  SEND_MESSAGE_INIT_FAILED: 'chat/sendMessageInitFailed',
  SAVE_MESSAGE_INIT: 'chat/saveMessageInit',
  SAVE_MESSAGE_INIT_SUCCESS: 'chat/saveMessageInitSuccess',
  SAVE_MESSAGE_INIT_FAILED: 'chat/saveMessageInitFailed',
  FINISH_CHAT_INIT: 'chat/finishChatInit',
  FINISH_CHAT_INIT_SUCCESS: 'chat/finishChatInitSuccess',
  FINISH_CHAT_INIT_FAILED: 'chat/finishChatInitFailed',
  ACTIVE_CHAT_INIT: 'chat/activeChatInit',
  ACTIVE_CHAT_INIT_SUCCESS: 'chat/activeChatInitSuccess',
  ACTIVE_CHAT_INIT_FAILED: 'chat/activeChatInitFailed'
}
