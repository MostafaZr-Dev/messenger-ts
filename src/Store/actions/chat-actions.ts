import ActionType from './ActionType'
import { chatActionTypes } from './Actions'

export const initNewChat = (userHash: string): ActionType => {
  return {
    type: chatActionTypes.INIT_NEW_CHAT,
    payload: {
      participant: userHash
    }
  }
}

export const newChatSuccess = (chatId: string, participants: object): ActionType => {
  return {
    type: chatActionTypes.INIT_NEW_CHAT_SUCCESS,
    payload: {
      id: chatId,
      participants
    }
  }
}

export const newChatFailed = (): ActionType => {
  return {
    type: chatActionTypes.INIT_NEW_CHAT_FAILED
  }
}

export const sendMessage = (data:any): ActionType => {
  return {
    type: chatActionTypes.SEND_MESSAGE_INIT,
    payload: {
      data
    }
  }
}

export const sendMessageSuccess = (data:any): ActionType => {
  return {
    type: chatActionTypes.SEND_MESSAGE_INIT_SUCCESS,
    payload: {
      data
    }
  }
}

export const sendMessageFailed = (): ActionType => {
  return {
    type: chatActionTypes.SEND_MESSAGE_INIT_FAILED
  }
}

export const saveMessage = (data:any): ActionType => {
  return {
    type: chatActionTypes.SAVE_MESSAGE_INIT,
    payload: {
      data
    }
  }
}

export const saveMessageSuccess = (data:any): ActionType => {
  return {
    type: chatActionTypes.SAVE_MESSAGE_INIT_SUCCESS,
    payload: {
      data
    }
  }
}

export const saveMessageFailed = (): ActionType => {
  return {
    type: chatActionTypes.SAVE_MESSAGE_INIT_FAILED
  }
}

export const finishChat = (chatID:string) : ActionType => {
  return {
    type: chatActionTypes.FINISH_CHAT_INIT,
    payload: {
      chatID
    }
  }
}

export const finishChatSuccess = () : ActionType => {
  return {
    type: chatActionTypes.FINISH_CHAT_INIT_SUCCESS
  }
}

export const finishChatFailed = () : ActionType => {
  return {
    type: chatActionTypes.FINISH_CHAT_INIT_FAILED
  }
}

export const activeChatInit = (chatID:string): ActionType => {
  return {
    type: chatActionTypes.ACTIVE_CHAT_INIT,
    payload: {
      chatID
    }
  }
}

export const activeChatInitSuccess = (currentChat:object, currentChatMessages: []): ActionType => {
  return {
    type: chatActionTypes.ACTIVE_CHAT_INIT_SUCCESS,
    payload: {
      currentChat,
      currentChatMessages
    }
  }
}

export const activeChatInitFailed = (): ActionType => {
  return {
    type: chatActionTypes.ACTIVE_CHAT_INIT_FAILED
  }
}
