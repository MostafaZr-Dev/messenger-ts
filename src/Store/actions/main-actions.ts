import { mainActionTypes } from '../actions/Actions'
import ActionType from './ActionType'
import IUser, { LocationType } from 'src/Contracts/IUser'
import IOnlineUser from '../../Contracts/IOnlineUser'
import IRecentChat from '../../Contracts/IRecentChat'

export const authUser = ():ActionType => {
  return {
    type: mainActionTypes.AUTH_USER
  }
}

export const authUserSuccess = (user: IUser, recentChats: IRecentChat[]):ActionType => {
  return {
    type: mainActionTypes.AUTH_USER_SUCCESS,
    payload: {
      user,
      recentChats
    }
  }
}

export const authUserFailed = ():ActionType => {
  return {
    type: mainActionTypes.AUTH_USER_FAILED
  }
}

export const setUserAddress = (address: string): ActionType => {
  return {
    type: mainActionTypes.USER_SET_ADDRESS,
    payload: {
      address
    }
  }
}

export const setUserAddressSuccess = (address:string, location: LocationType): ActionType => {
  return {
    type: mainActionTypes.USER_SET_ADDRESS_SUCCESS,
    payload: {
      address,
      location
    }
  }
}

export const setUserAddressFailed = (): ActionType => {
  return {
    type: mainActionTypes.USER_SET_ADDRESS_FAILED
  }
}

export const updateOnlineUsers = (onlineUsers: IOnlineUser[]):ActionType => {
  return {
    type: mainActionTypes.UPDATE_ONLINE_USERS,
    payload: {
      onlineUsers
    }
  }
}

export const updateOnlineUsersSuccess = (onlineUsers: IOnlineUser[]):ActionType => {
  return {
    type: mainActionTypes.UPDATE_ONLINE_USERS_SUCCESS,
    payload: {
      onlineUsers
    }
  }
}

export const logoutRequest = () => {
  return {
    type: mainActionTypes.LOGOUT_REQUEST
  }
}

export const logoutRequestSuccess = () => {
  return {
    type: mainActionTypes.LOGOUT_REQUEST_SUCCESS
  }
}

export const logoutRequestFailed = () => {
  return {
    type: mainActionTypes.LOGOUT_REQUEST_FAILED
  }
}
