import ActionType from './ActionType'
import { userActionTypes } from './Actions'

export const apiInit = () => {
  return {
    type: userActionTypes.API_INIT
  }
}

export const userLogin = (email: string, password: string): ActionType => {
  return {
    type: userActionTypes.USER_LOGIN,
    payload: {
      email,
      password
    }
  }
}

export const userLoginSuccess = (message:string): ActionType => {
  return {
    type: userActionTypes.USER_LOGIN_SUCCESS,
    payload: {
      message
    }
  }
}

export const userLoginFailed = (message: any): ActionType => {
  return {
    type: userActionTypes.USER_LOGIN_FAILED,
    payload: {
      message
    }
  }
}

export const userRegister = (fullName:string, email: string, password: string): ActionType => {
  return {
    type: userActionTypes.USER_REGISTER,
    payload: {
      fullName,
      email,
      password
    }
  }
}

export const userRegisterSuccess = (message: string): ActionType => {
  return {
    type: userActionTypes.USER_REGISTER_SUCCESS,
    payload: {
      message
    }
  }
}

export const userRegisterFailed = (): ActionType => {
  return {
    type: userActionTypes.USER_REGISTER_FAILED,
    payload: null
  }
}
