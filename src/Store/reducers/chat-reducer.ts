import ActionType from '../actions/ActionType'
import { chatActionTypes } from '../actions/Actions'

type ChatState = {
    currentChat: object | null;
    currentChatMessages: object[];
}

const initState: ChatState = {
  currentChat: null,
  currentChatMessages: []
}

const chatReducer = (state = initState, action:ActionType) => {
  switch (action.type) {
    case chatActionTypes.INIT_NEW_CHAT_SUCCESS:
      return {
        ...state,
        currentChat: action.payload
      }
    case chatActionTypes.SEND_MESSAGE_INIT_SUCCESS:
      return {
        ...state,
        currentChatMessages: [
          ...state.currentChatMessages,
          { ...action.payload.data }
        ]
      }
    case chatActionTypes.SEND_MESSAGE_INIT_FAILED:
      return {
        ...state
      }
    case chatActionTypes.SAVE_MESSAGE_INIT_SUCCESS:
      return {
        ...state,
        currentChatMessages: [
          ...state.currentChatMessages,
          { ...action.payload.data }
        ]
      }
    case chatActionTypes.SAVE_MESSAGE_INIT_FAILED:
      return {
        ...state
      }
    case chatActionTypes.FINISH_CHAT_INIT_SUCCESS:
      return {
        ...state,
        currentChat: null,
        currentChatMessages: []
      }
    case chatActionTypes.FINISH_CHAT_INIT_FAILED:
      return {
        ...state
      }

    case chatActionTypes.ACTIVE_CHAT_INIT_SUCCESS:
      return {
        ...state,
        currentChat: action.payload.currentChat,
        currentChatMessages: action.payload.currentChatMessages
      }
    case chatActionTypes.ACTIVE_CHAT_INIT_FAILED:
      return {
        ...state
      }
    default:
      return state
  }
}

export default chatReducer
