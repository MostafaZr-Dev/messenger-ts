import { combineReducers } from 'redux'

import mainReducer from './main-reducer'
import usersReducer from './users-reducer'
import chatReducer from './chat-reducer'

export default combineReducers({
  main: mainReducer,
  users: usersReducer,
  chat: chatReducer
})
