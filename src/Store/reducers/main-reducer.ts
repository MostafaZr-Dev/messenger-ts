import ActionType from '../actions/ActionType'
import IUser from '../../Contracts/IUser'
import IOnlineUser from '../../Contracts/IOnlineUser'
import { mainActionTypes } from '../actions/Actions'
import IRecentChat from '../../Contracts/IRecentChat'

type MainState = {
    apiState: {
      isLoading: boolean;
      success: boolean;
      error: boolean;
      message: string | null;
    };
    isAuth: boolean;
    isUserLoggedIn: boolean;
    me:IUser | null;
    onlineUsers: IOnlineUser[];
    recentChats:IRecentChat[];
}

const initState: MainState = {
  apiState: {
    isLoading: false,
    success: false,
    error: false,
    message: null
  },
  isAuth: true,
  isUserLoggedIn: false,
  me: null,
  onlineUsers: [],
  recentChats: []
}

const mainReducer = (state = initState, action:ActionType) => {
  switch (action.type) {
    case mainActionTypes.API_ISLOADING:
      return {
        ...state,
        apiState: {
          ...state.apiState,
          isLoading: true
        }
      }
    case mainActionTypes.AUTH_USER_SUCCESS:
      return {
        ...state,
        isAuth: false,
        isUserLoggedIn: true,
        me: {
          ...action.payload.user,
          location: null
        },
        recentChats: action.payload.recentChats
      }
    case mainActionTypes.AUTH_USER_FAILED:
      return {
        ...state,
        isAuth: false,
        isUserLoggedIn: false,
        me: null
      }
    case mainActionTypes.USER_SET_ADDRESS_SUCCESS:
      return {
        ...state,
        apiState: {
          ...state.apiState,
          isLoading: false
        },
        me: {
          ...state.me,
          address: action.payload.address,
          location: action.payload.location
        }
      }
    case mainActionTypes.USER_SET_ADDRESS_FAILED:
      return {
        ...state,
        apiState: {
          ...state.apiState,
          isLoading: false
        }
      }
    case mainActionTypes.UPDATE_ONLINE_USERS_SUCCESS:
      return {
        ...state,
        onlineUsers: action.payload.onlineUsers
      }
    case mainActionTypes.LOGOUT_REQUEST_SUCCESS:
      return {
        ...state,
        isUserLoggedIn: false
      }
    case mainActionTypes.LOGOUT_REQUEST_FAILED:
      return {
        ...state
      }
    default:
      return state
  }
}

export default mainReducer
