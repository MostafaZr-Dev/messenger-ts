import ActionType from '../actions/ActionType'
import { userActionTypes } from '../actions/Actions'

export type UsersState = {
  apiState: {
    success: boolean;
    error: boolean;
    message: string | null;
  };
}

const initState: UsersState = {
  apiState: {
    success: false,
    error: false,
    message: null
  }
}

const usersReducer = (state = initState, action: ActionType) => {
  switch (action.type) {
    case userActionTypes.API_INIT:
      return {
        ...state,
        apiState: {
          success: false,
          error: false,
          message: null
        }
      }
    case userActionTypes.USER_REGISTER_SUCCESS:
      return {
        ...state,
        apiState: {
          ...state.apiState,
          success: true,
          message: action.payload.message
        }
      }
    case userActionTypes.USER_REGISTER_FAILED:
      return {
        ...state,
        apiState: {
          ...state.apiState,
          success: false,
          error: true
        }
      }
    case userActionTypes.USER_LOGIN_SUCCESS:
      return {
        ...state,
        apiState: {
          ...state.apiState,
          success: true,
          message: action.payload.message
        }
      }
    case userActionTypes.USER_LOGIN_FAILED:
      return {
        ...state,
        apiState: {
          ...state.apiState,
          success: false,
          error: true,
          message: action.payload.message
        }
      }
    default:
      return state
  }
}

export default usersReducer
