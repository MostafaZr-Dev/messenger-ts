import { call, takeLatest, SagaReturnType, put } from 'redux-saga/effects'

import ActionType from '../actions/ActionType'
import { chatActionTypes } from '../actions/Actions'
import ChatService from '../../Services/ChatService'
import {
  newChatSuccess,
  newChatFailed,
  sendMessageSuccess,
  sendMessageFailed,
  saveMessageSuccess,
  saveMessageFailed,
  finishChatSuccess,
  finishChatFailed,
  activeChatInitFailed,
  activeChatInitSuccess
} from '../actions/chat-actions'

const chatService = new ChatService()

type NewChatResult = SagaReturnType<typeof chatService.newChat>
function * initNewChatWorker (action: ActionType) {
  try {
    const token: string = yield call(() => {
      return localStorage.getItem('token')
    })

    const { participant } = action.payload

    const newChatResponse: NewChatResult = yield call(chatService.newChat, token, participant)

    const { id, participants: participantsDetails } = newChatResponse.data.chat
    console.log(newChatResponse, id, participantsDetails)
    yield put(newChatSuccess(id, participantsDetails))
  } catch (error) {
    yield put(newChatFailed())
  }
}

function * sendMessageWorker (action: ActionType) {
  try {
    yield put(sendMessageSuccess(action.payload.data))
  } catch (error) {
    yield put(sendMessageFailed())
  }
}

type SaveMessageResult = SagaReturnType<typeof chatService.saveMessage>
function * saveMessageWorker (action: ActionType) {
  try {
    const token:string = yield call(() => {
      return localStorage.getItem('token')
    })

    const saveMessageResponse: SaveMessageResult = yield call(
      chatService.saveMessage,
      token,
      action.payload.data
    )

    console.log(saveMessageResponse)

    yield put(saveMessageSuccess(action.payload.data))
  } catch (error) {
    yield put(saveMessageFailed())
  }
}

type FinishChatResult = SagaReturnType<typeof chatService.finishChat>
function * finishChatWorker (action:ActionType) {
  try {
    const { chatID } = action.payload

    const token:string = yield call(() => {
      return localStorage.getItem('token')
    })

    const finishChatResponse:FinishChatResult = yield call(
      chatService.finishChat,
      token,
      chatID
    )

    console.log({ chatID, finishChatResponse })

    yield put(finishChatSuccess())
  } catch (error) {
    yield put(finishChatFailed())
  }
}

type ActiveChatResult = SagaReturnType<typeof chatService.fetchActiveChat>
function * activeChatWorker (action: ActionType) {
  try {
    const { chatID } = action.payload

    const token:string = yield call(() => {
      return localStorage.getItem('token')
    })

    const activeChatResponse:ActiveChatResult = yield call(
      chatService.fetchActiveChat,
      token,
      chatID
    )

    const { currentChat, currentChatMessages } = activeChatResponse.data.chat

    yield put(activeChatInitSuccess(currentChat, currentChatMessages))
  } catch (error) {
    yield put(activeChatInitFailed())
  }
}

export const initNewChatWatcher = function * () {
  yield takeLatest(chatActionTypes.INIT_NEW_CHAT, initNewChatWorker)
}

export const sendMessageWatcher = function * () {
  yield takeLatest(chatActionTypes.SEND_MESSAGE_INIT, sendMessageWorker)
}

export const saveMessageWatcher = function * () {
  yield takeLatest(chatActionTypes.SAVE_MESSAGE_INIT, saveMessageWorker)
}

export const finishChatWatcher = function * () {
  yield takeLatest(chatActionTypes.FINISH_CHAT_INIT, finishChatWorker)
}

export const activeChatWatcher = function * () {
  yield takeLatest(chatActionTypes.ACTIVE_CHAT_INIT, activeChatWorker)
}
