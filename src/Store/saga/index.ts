import makeSagaMiddleware from 'redux-saga'

export default makeSagaMiddleware()
export { default as sagaApi } from './watchers'
