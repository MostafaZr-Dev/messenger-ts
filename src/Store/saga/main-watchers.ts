// eslint-disable-next-line no-unused-vars
import { takeEvery, takeLatest, put, call, SagaReturnType } from 'redux-saga/effects'

import IUser from '../../Contracts/IUser'
import ActionType from '../actions/ActionType'
import { mainActionTypes } from '../actions/Actions'
import {
  authUserFailed, authUserSuccess, logoutRequestFailed, logoutRequestSuccess, setUserAddressFailed, setUserAddressSuccess, updateOnlineUsersSuccess
} from '../actions/main-actions'
import AuthService from '../../Services/AuthService'
import ClientService from '../../Services/ClientService'

const authService = new AuthService()
const clientService = new ClientService()

type AuthResult = SagaReturnType<typeof authService.authenticate>
export const authenticateWorker = function * (action:ActionType) {
  try {
    const token: string | null = yield call(() => {
      return localStorage.getItem('token')
    })

    const authResponse: AuthResult = yield call(authService.authenticate, token as string)

    const { user, recentChats } = authResponse.data

    yield put(authUserSuccess(user as IUser, recentChats))
  } catch (error) {
    yield put(authUserFailed())
  }
}

type SetAddressResult = SagaReturnType<typeof clientService.setAddress>
// type SetLocationResult = SagaReturnType<typeof clientService.setLocation>
function * setUserAddressWorker (action:ActionType) {
  try {
    const { address } = action.payload

    yield put({
      type: mainActionTypes.API_ISLOADING
    })

    const setAddressResponse: SetAddressResult = yield call(
      clientService.setAddress,
      address
    )

    if (setAddressResponse.data.status === 'OK') {
      const { location } = setAddressResponse.data

      yield put(setUserAddressSuccess(address, {
        latitude: location.y,
        longitude: location.x
      }))
    }
  } catch (error) {
    console.log(error)
    yield put(setUserAddressFailed())
  }
}

function * updateOnlineUsersWorker (action:ActionType) {
  try {
    const { onlineUsers } = action.payload

    yield put(updateOnlineUsersSuccess(onlineUsers))
  } catch (error) {

  }
}

function * logoutRequestWorker (action:ActionType) {
  try {
    yield call(() => {
      localStorage.removeItem('token')
    })

    yield put(logoutRequestSuccess())
  } catch (error) {
    yield put(logoutRequestFailed())
  }
}

export const authenticateWatcher = function * () {
  yield takeLatest(mainActionTypes.AUTH_USER, authenticateWorker)
}

export const setUserAddressWatcher = function * () {
  yield takeLatest(mainActionTypes.USER_SET_ADDRESS, setUserAddressWorker)
}

export const updateOnlineUsersWatcher = function * () {
  yield takeLatest(mainActionTypes.UPDATE_ONLINE_USERS, updateOnlineUsersWorker)
}

export const logoutRequestWatcher = function * () {
  yield takeLatest(mainActionTypes.LOGOUT_REQUEST, logoutRequestWorker)
}
