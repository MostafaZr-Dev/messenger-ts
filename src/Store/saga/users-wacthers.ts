// eslint-disable-next-line no-unused-vars
import { takeEvery, takeLatest, put, call, SagaReturnType } from 'redux-saga/effects'

import ActionType from '../actions/ActionType'
import { userActionTypes } from '../actions/Actions'
import {
  userRegisterFailed,
  userRegisterSuccess,
  userLoginSuccess,
  userLoginFailed,
  apiInit
} from '../actions/user-actions'
import AuthService from '../../Services/AuthService'

const authService = new AuthService()

type RegisterResult = SagaReturnType<typeof authService.register>
function * userRegisterWorker (action: ActionType) {
  try {
    const { fullName, email, password } = action.payload

    const registerResponse: RegisterResult = yield call(authService.register, fullName, email, password)

    yield put(userRegisterSuccess(registerResponse.data.message))
  } catch (error) {
    yield put(userRegisterFailed())
  }
}

type LoginResult = SagaReturnType<typeof authService.login>
function * userLoginWorker (action: ActionType) {
  try {
    const { email, password } = action.payload
    yield put(apiInit())

    const loginResponse: LoginResult = yield call(authService.login, email, password)

    const { message, token } = loginResponse.data
    yield put(userLoginSuccess(message))
    yield call(() => {
      localStorage.setItem('token', token as string)
    })
  } catch (error) {
    yield put(userLoginFailed(error.response.data.message))
  }
}

export const userRegisterWatcher = function * () {
  yield takeLatest(userActionTypes.USER_REGISTER, userRegisterWorker)
}

export const userLoginWatcher = function * () {
  yield takeLatest(userActionTypes.USER_LOGIN, userLoginWorker)
}
