import { all } from 'redux-saga/effects'

import * as userWatchers from './users-wacthers'
import * as mainWatchers from './main-watchers'
import * as chatWatchers from './chat-watchers'

export default function * root () {
  yield all([
    mainWatchers.authenticateWatcher(),
    mainWatchers.setUserAddressWatcher(),
    mainWatchers.updateOnlineUsersWatcher(),
    mainWatchers.logoutRequestWatcher(),
    userWatchers.userRegisterWatcher(),
    userWatchers.userLoginWatcher(),
    chatWatchers.initNewChatWatcher(),
    chatWatchers.sendMessageWatcher(),
    chatWatchers.saveMessageWatcher(),
    chatWatchers.finishChatWatcher(),
    chatWatchers.activeChatWatcher()
  ])
}
