
export const selectCurrentChat = (state:any) => state.chat.currentChat

export const selectCurrentChatID = (state: any) => state.chat.currentChat.id

export const selectOtherParticipant = (state:any) => {
  const { currentChat } = state.chat
  const { me } = state.main
  console.log(currentChat)
  const otherParticipantHash = Object.keys(currentChat.participants)
    .filter((hash: string) => hash !== me.hash).pop()

  return otherParticipantHash ? currentChat.participants[otherParticipantHash] : {}
}

export const selectCurrentChatMessages = (state:any) => state.chat.currentChatMessages
