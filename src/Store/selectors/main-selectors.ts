import IOnlineUser from 'src/Contracts/IOnlineUser'

export const selectAuthFlag = (state: any) => state.main.isAuth

export const selectLogginFlag = (state: any) => state.main.isUserLoggedIn

export const selectUserLocation = (state: any) => state.main.me?.location

export const selectCurrentUser = (state: any) => state.main.me

export const selectOnlineUsers = (state: any) => {
  const { me, onlineUsers } = state.main
  const meHash = me.hash
  console.log(onlineUsers, meHash)
  const others: IOnlineUser[] = onlineUsers.filter(
    (onlineUser: IOnlineUser) => onlineUser.user.hash !== meHash
  )

  return others
}

export const selectRecentChats = (state:any) => state.main.recentChats
